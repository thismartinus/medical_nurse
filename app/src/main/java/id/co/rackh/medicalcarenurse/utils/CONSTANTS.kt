package id.co.rackh.medicalcarenurse.utils

const val APP_NAME: String = "Medical Care"
const val USER_PREFERENCE_NAME: String = "co.id.rackh.smilestudio.user.preference"
const val USER_PREFERENCE_HAS_LOGIN_TAG: String = "co.id.rackh.smilestudio.user.preference.has.login"
const val USER_PREFERENCE_USER_DATA: String = "co.id.rackh.smilestudio.user.data"
const val BASE_NETWORK_SERVICE_URL: String = "co.id.rackh.smilestudio.baseurl.preferences.tag"
const val NETWORK_BASE_PREFERENCE_NAME: String = "co.id.rackh.smilestudio.network.preference"
const val NEED_TUTORIAL_PREFERENCE_NAME: String = "co.id.rackh.smilestudio.tutorial.needed"
const val RESERVATION_PREFERENCE_NAME: String = "co.id.rackh.smilestudio.reservation.reminder"
const val USER_RESERVATION_DATA: String = "co.id.rackh.smilestudio.reservation.user"
const val USER_HAS_RESERVATION: String ="co.id.rackh.smilestudio.has.reservation"