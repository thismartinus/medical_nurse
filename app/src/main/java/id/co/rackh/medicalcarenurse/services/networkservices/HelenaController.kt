// alias NetworkController

package id.co.rackh.medicalcarenurse.services.networkservices

import com.android.volley.VolleyError
import org.json.JSONObject

class HelenaController(networkInterface: NetworkInterface): NetworkInterface {
    private val network: NetworkInterface = networkInterface
    override fun get(
        url: String,
        token: String?,
        completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit
    ) {
        network.get(url, token, completionHandler)
    }

    override fun post(
        url: String,
        body: JSONObject,
        token: String?,
        completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit
    ) {
        network.post(url, body, token, completionHandler)
    }
}