package id.co.rackh.medicalcarenurse.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.rackh.medicalcarenurse.R

class HistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
    }
}
