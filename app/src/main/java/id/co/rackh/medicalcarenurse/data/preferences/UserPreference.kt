package id.co.rackh.medicalcarenurse.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.co.rackh.medicalcarenurse.data.model.User
import id.co.rackh.medicalcarenurse.utils.NEED_TUTORIAL_PREFERENCE_NAME
import id.co.rackh.medicalcarenurse.utils.USER_PREFERENCE_HAS_LOGIN_TAG
import id.co.rackh.medicalcarenurse.utils.USER_PREFERENCE_NAME
import id.co.rackh.medicalcarenurse.utils.USER_PREFERENCE_USER_DATA
import java.lang.reflect.Type

class UserPreference(private val context: Context) {
    private var sharedPreferences: SharedPreferences? = null

    fun storeUserData(data: User) {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        sharedPreferences?.edit()?.putString(USER_PREFERENCE_USER_DATA, Gson().toJson(data))?.apply()
    }

    fun getUserData(): User {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        val type: Type = object : TypeToken<User>() { }.type
        return Gson().fromJson(sharedPreferences?.getString(USER_PREFERENCE_USER_DATA, null), type)
    }

    fun setUserLoggedIn() {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        sharedPreferences?.edit()?.putBoolean(USER_PREFERENCE_HAS_LOGIN_TAG, true)?.apply()
    }

    fun setTutorialcomplete() {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        sharedPreferences?.edit()?.putBoolean(NEED_TUTORIAL_PREFERENCE_NAME, false)?.apply()
    }

    fun editUserInformation(user: User) {
        storeUserData(user)
    }

    fun getIsUserLoggedIn(): Boolean {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        return sharedPreferences!!.getBoolean(USER_PREFERENCE_HAS_LOGIN_TAG, false)
    }

    fun needTutorial(): Boolean {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        return sharedPreferences!!.getBoolean(NEED_TUTORIAL_PREFERENCE_NAME, true)
    }

    fun clearCache() {
        sharedPreferences = context.getSharedPreferences(USER_PREFERENCE_NAME, Context.MODE_PRIVATE)
        sharedPreferences?.edit()?.clear()?.apply()
    }
}