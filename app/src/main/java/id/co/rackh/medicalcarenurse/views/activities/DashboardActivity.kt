package id.co.rackh.medicalcarenurse.views.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import id.co.rackh.medicalcarenurse.R
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.jetbrains.anko.toast

class DashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(myAppBar)

        approve.setOnClickListener {
            val intent = Intent(this@DashboardActivity, RentrequestActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            R.id.profile_nav -> {
                startActivity(Intent(this@DashboardActivity, ProfileActivity::class.java))

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_appbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}
