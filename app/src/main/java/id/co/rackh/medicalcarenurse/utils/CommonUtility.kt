package id.co.rackh.medicalcarenurse.utils

import android.annotation.SuppressLint
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import android.util.Log
import com.android.volley.*
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import java.lang.reflect.Field

class CommonUtility {

    companion object {
        val allowedAppsPackage: List<String> = listOf(
                "com.google.android.gm",
                "com.whatsapp",
                "com.google.android.talk"
        )
        val messageDefaultUri: String = "sms:"
    }

    /**
     * Used to get the response message for each network error
     * @param err [VolleyError] your volley error
     * @return [String] error message
     */
    fun volleyNetworkErrorHandling(err: VolleyError): String {
        var mMessageResult = "Couldn't make the request"
        if ( err is ServerError) {
            mMessageResult = "Couldn't process the request"
        } else if ( err is ParseError) {
            mMessageResult = "Error in parsing data. Please try it again later"
        } else if ( err is AuthFailureError) {
            mMessageResult = "Please check your information data again"
        } else if ( err is TimeoutError || err is NetworkError ) {
            mMessageResult = "Please check your internet connection"
        }
        return mMessageResult
    }

    /**
     *
     */
    @SuppressLint("RestrictedApi")
    fun removeShiftModeBottomNavigation(view: BottomNavigationView) {
        val menuView: BottomNavigationMenuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode: Field = menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for ( i in 0 until menuView.childCount ) {
                val item: BottomNavigationItemView = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShifting(false)
                item.setChecked(item.itemData.isChecked)
            }

        } catch (error: Exception) {
            Log.e("ERR", "Failed to change the nav bottom")
        }
    }

}