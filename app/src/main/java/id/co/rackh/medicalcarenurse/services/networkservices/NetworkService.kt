package id.co.rackh.medicalcarenurse.services.networkservices

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import id.co.rackh.medicalcarenurse.AppDelegate
import org.json.JSONObject

class NetworkService: NetworkInterface {

    companion object {
        const val baseURL = "https://backend.medicalcareindonesia.com/"
    }

    override fun get(
        url: String,
        token: String?,
        completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit
    ) {
        val request = object : JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject> {
                response: JSONObject? ->
                completionHandler(response, null)
            },
            Response.ErrorListener {
                error: VolleyError? ->
                completionHandler(null, error)
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = if ( token != null ) {
                    "Bearer $token"
                } else {
                    ""
                }
                return params
            }
        }
        AppDelegate.instance?.addToRequestQueue(request, AppDelegate::class.java.simpleName)
    }

    override fun post(
        url: String,
        body: JSONObject,
        token: String?,
        completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit
    ) {
        val request = object: JsonObjectRequest(Request.Method.POST, url, body,
            Response.Listener {
                response: JSONObject? ->
                completionHandler(response, null)
            },
            Response.ErrorListener {
                error: VolleyError ->
                completionHandler(null, error)
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = if ( token != null ) {
                    "Bearer $token"
                } else {
                    ""
                }
                return params
            }
        }
    }
}