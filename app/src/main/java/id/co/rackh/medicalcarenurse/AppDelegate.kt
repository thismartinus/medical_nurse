package id.co.rackh.medicalcarenurse

import android.app.Application
import android.text.TextUtils
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class AppDelegate: Application() {
    override fun onCreate() {
        super.onCreate()
    }

    var requestQueue: RequestQueue? = null
        get(){
            if(field == null){
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }
        fun <T> addToRequestQueue(request: Request<T>, tag: String){
            request.tag= if(TextUtils.isEmpty(tag)) TAG else tag
            requestQueue?.add(request)
        }
        fun <T> addToRequestQueque(request: Request<T>){
            request.tag = TAG
            requestQueue?.add(request)
        }
        fun cencelPendingRequest(tag: Any){
            if(requestQueue != null){
                requestQueue!!.cancelAll(tag)
            }
        }

        companion object {
            private val TAG: String = Application::class.java.simpleName
            @get:Synchronized var instance: AppDelegate? = null
                private set
        }
}