package id.co.rackh.medicalcarenurse.data.model

data class User (var id: Int, var name: String = "",
                 var api_token: String = "", var device_id: String = "",
                 var id_customer: Int, var address: String = "", var date_of_birth: String = "", var phone: String,
                 var photo: String = "", var auth_code: String = "", var email: String = "", var blood_type: String = "",
                 var height: String = "", var weight: String = "", var gender: String = "")