package id.co.rackh.medicalcarenurse.views.activities


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.co.rackh.medicalcarenurse.R

class SplashScreenActivity : AppCompatActivity() {

    private var delay: Long =  3000
    private var delayHandler: Handler? = null

    internal val mRunnable: Runnable = Runnable{
        if(!isFinishing){
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        delayHandler = Handler()

        delayHandler!!.postDelayed(mRunnable, delay)

    }

    public override fun onDestroy() {
        if(delayHandler != null ){
            delayHandler!!. removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}
