package id.co.rackh.medicalcarenurse.services.networkservices

import com.android.volley.Response
import com.android.volley.VolleyError
import org.json.JSONObject


interface NetworkInterface {
    fun get(url: String, token: String? , completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit)
    fun post(url: String, body: JSONObject, token: String?, completionHandler: (response: JSONObject?, error: VolleyError?) -> Unit)
}