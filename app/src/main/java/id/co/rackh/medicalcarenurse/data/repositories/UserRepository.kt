package id.co.rackh.medicalcarenurse.data.repositories

import android.app.Application
import com.android.volley.VolleyError
import id.co.rackh.medicalcarenurse.data.model.User
import id.co.rackh.medicalcarenurse.data.preferences.UserPreference
import id.co.rackh.medicalcarenurse.services.networkservices.HelenaController
import id.co.rackh.medicalcarenurse.services.networkservices.NetworkService
import id.co.rackh.medicalcarenurse.utils.CommonUtility
import org.json.JSONObject

class UserRepository(application: Application) {
    private val utility = CommonUtility()
    private val networkService: NetworkService = NetworkService()
    private var networkController: HelenaController
    private val userPreference = UserPreference(application)

    companion object {
        const val LOGIN_ENDPOINT = "api/login"
    }

    init {
        networkController = HelenaController(networkService)
    }

    fun login(email: String, password: String, completion: (status: Boolean, message: String) -> Unit) {
        val param = JSONObject()
        param.put("email", email)
        param.put("password", password)
        networkController.post(LOGIN_ENDPOINT, param, null) {
                response: JSONObject?, error: VolleyError? ->
            if ( response != null && response.has("user") ) {
                val userJSON = response.getJSONObject("user")
                val customerJSON = userJSON.getJSONObject("customer")
                val token = response.getString("token")
                val user = User(
                    id = userJSON.getInt("id"),
                    name = userJSON.getString("name"),
                    api_token = token,
                    device_id = "",
                    id_customer = customerJSON.getInt("id"),
                    address = customerJSON.getString("address"),
                    date_of_birth = if ( customerJSON.getString("date_of_birth") == null
                        || customerJSON.getString("date_of_birth") == "null" ) {
                        "Unknown"
                    } else { customerJSON.getString("date_of_birth") },
                    phone = customerJSON.getString("phone"),
                    photo = NetworkService.baseURL + customerJSON.getString("photo"),
                    email = userJSON.getString("email"),
                    blood_type = if ( customerJSON.getString("blood_type") == "null"
                        || customerJSON.getString("blood_type") == null ) {
                        "Unknown"
                    } else {
                        customerJSON.getString("blood_type")
                    },
                    height = if ( customerJSON.getString("height") == "null"
                        || customerJSON.getString("height") == null ) {
                        "Unknown"
                    } else {
                        customerJSON.getString("height")
                    },
                    weight = if ( customerJSON.getString("weight") == "null"
                        || customerJSON.getString("weight") == null ) {
                        "Unknown"
                    } else {
                        customerJSON.getString("weight")
                    },
                    gender = if ( customerJSON.getString("gender") == "null"
                        || customerJSON.getString("gender") == null ) {
                        "Unknown"
                    } else {
                        customerJSON.getString("gender")
                    })
                userPreference.storeUserData(user)
                userPreference.setUserLoggedIn()
                completion(true, "Welcome to Medical Care")
            } else {
                val message = utility.volleyNetworkErrorHandling(error!!)
                completion(false, message)
            }
        }
    }
}